import './App.css';
import Navbar from './components/Navbar';
import { Route, Routes } from 'react-router-dom';
import Loginpage from './components/Loginpage';
import 'bootstrap/dist/css/bootstrap.min.css';
import Signuppage from './components/Signuppage';

function App() {
  return (
    <>

      <Navbar />
      <Routes>
        <Route path='Loginpage' element={<Loginpage />} />
        <Route path='Signuppage' element={<Signuppage />} />
      </Routes>
     

    </>


  );
}

export default App;
