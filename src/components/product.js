import React, { useState } from 'react';

const productsData = [
{
    id: 1,
    name: 'Product 1',
    price: 10,
    image: 'https://example.com/product1.jpg',
},
{
    id: 2,
    name: 'Product 2',
    price: 20,
    image: 'https://example.com/product2.jpg',
},
{
    id: 3,
    name: 'Product 3',
    price: 30,
    image: 'https://example.com/product3.jpg',
},
];

const ProductCard = ({ product, addToCart }) => {
const { id, name, price, image } = product;

const handleAddToCart = () => {
    addToCart(product);
};

return (
    <div className="card">
    <img src={image} alt={name} />
    <h3>{name}</h3>
    <p>Price: ${price}</p>
    <button onClick={handleAddToCart}>Add to Cart</button>
    </div>
);
};

const Cart = ({ cartItems, removeItem, processOrder }) => {
const [address, setAddress] = useState('');

const handleAddressChange = (event) => {
    setAddress(event.target.value);
};

const handleRemoveItem = (itemId) => {
    removeItem(itemId);
};

const handleProcessOrder = () => {
    processOrder(address);
};

return (
    <div>
    <h2>Cart</h2>
    {cartItems.length === 0 ? (
        <p>Your cart is empty.</p>
    ) : (
        <div>
        <ul>
            {cartItems.map((item) => (
            <li key={item.id}>
                {item.name} - ${item.price}
                <button onClick={() => handleRemoveItem(item.id)}>
                Remove
                </button>
            </li>
            ))}
        </ul>
        <div>
            <input
            type="text"
            placeholder="Enter address"
            value={address}
            onChange={handleAddressChange}
            />
            <button onClick={handleProcessOrder}>Complete Order</button>
        </div>
        </div>
    )}
    </div>
);
};

const ProductList = () => {
const [cartItems, setCartItems] = useState([]);

const addToCart = (product) => {
    setCartItems([...cartItems, product]);
};

const removeItem = (itemId) => {
    const updatedCartItems = cartItems.filter((item) => item.id !== itemId);
    setCartItems(updatedCartItems);
};

const processOrder = (address) => {
    // Perform order processing logic here
    console.log('Order processed with address:', address);
    setCartItems([]);
};

return (
    <div>
    <h2>Product List</h2>
    <div className="product-list">
        {productsData.map((product) => (
        <ProductCard
            key={product.id}
            product={product}
            addToCart={addToCart}
        />
        ))}
    </div>
    <Cart
        cartItems={cartItems}
        removeItem={removeItem}
        processOrder={processOrder}
    />
    </div>
);
};

export default ProductList;
