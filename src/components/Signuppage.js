import React, { useState } from 'react';
import './SignupPage.css'; // Import your CSS file
import { FaLock } from 'react-icons/fa';

const SignupPage = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');

  const handleSignup = async (e) => {
    e.preventDefault();

    if (!email || !password || !confirmPassword || !firstName || !lastName || !phoneNumber) {
      console.log('Please fill in all fields');
      return;
    }

    if (password !== confirmPassword) {
      console.log('Passwords do not match');
      return;
    }
    const user = {
      email,
      password,
      firstName,
      lastName,
      phoneNumber,
    };

    try {
      // Send the signup request to the server
      const response = await fetch('http://localhost:8080/api/auth/signup', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(user),
      });

      // Check the response status
      if (response.ok) {
        // Signup successful, handle the response accordingly
        console.log('Signup successful');
      } else {
        // Signup failed, handle the error response
        const errorData = await response.json();
        console.log('Signup failed:', errorData.message);
      }
    } catch (error) {
      // An error occurred while making the request
      console.log('Error:', error.message);
    }
  };

  return (
     
    <div className="signup-container">
      <div className="lock-container">
        <FaLock className="lock-icon"/>
      </div>
      
      <h2>Sign Up</h2>
      <form>
        <input
          type="text"
          placeholder="First Name"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
        />

        <input
          type="text"
          placeholder="Last Name"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
        />


        <input
          type="email"
          placeholder="Email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <input
          type="tel"
          placeholder="Phone Number"
          value={phoneNumber}
          onChange={(e) => setPhoneNumber(e.target.value)}
        />
        <input
          type="password"
          placeholder="Password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <input
          type="password"
          placeholder="Confirm Password"
          value={confirmPassword}
          onChange={(e) => setConfirmPassword(e.target.value)}
        />

        <button type="submit" onClick={handleSignup}>
          Sign Up
        </button>
      </form>
      <p className="login-link">
        Already have an account? <a href="/loginpage">Click here</a> to login.
      </p>
      <footer className="footer">
      <p>&copy; 2023 UpGrad E-Shop. All rights reserved.</p>
    </footer>
    </div>

   
  );
};

export default SignupPage;
