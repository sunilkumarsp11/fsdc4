 
import React, { useState } from 'react';
import './Loginpage.css';
import { FaLock } from 'react-icons/fa';

const Loginpage = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');

  const handleLogin = (event) => {
    event.preventDefault();

    fetch('http://localhost:8000/api/auth/signin', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ username, password }),
    })
      .then(response => {
        if (response.ok) {
          // Successful login, perform necessary actions
          // For example, redirect the user to a new page
          window.location.href = '/Productpage';
        } else {
          throw new Error('Invalid credentials');
        }
      })
      .catch(error => {
        setError(error.message);
      });
  };

  return (
    <div className="signup-container">
      <div className="lock-container">
        <FaLock className="lock-icon"/>
      </div>
      <h2>Sign In</h2>      
      <form onSubmit={handleLogin}>
        <input
        
          type="text"
          placeholder="Username"
          value={username}
          onChange={event => setUsername(event.target.value)}
        />
        <>  </>   
        <br></br>
        <br></br>
        <input
         
          type="password"
          placeholder="Password"
          value={password}
          onChange={event => setPassword(event.target.value)}
        />
         <>  </>   
        <br></br>
        <br></br>
        <button   type="submit">Login</button>
      </form>
      <p className="login-link">
        Don't have an account?Sign up <a href="/SignupPage">Click here</a> to login.
      </p>
     
      {error && <p className="error-message">{error}</p>}
      <footer className="footer">
      <p>&copy; 2023 UpGrad E-Shop. All rights reserved.</p>
    </footer>
    </div>
    
    
  );
};

export default Loginpage;
