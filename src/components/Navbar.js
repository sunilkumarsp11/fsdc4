import React, { useState } from 'react';
import { IconButton, Toolbar, Typography, AppBar, InputBase } from '@mui/material';
import { ShoppingCart } from '@mui/icons-material';
import './Navbar.css';
import SearchIcon from '@mui/icons-material/Search';
import { styled, alpha } from '@mui/material/styles';
import { Link } from 'react-router-dom';

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 2, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginRight: theme.spacing(80),
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(6),
    width: 'auto',
  },
}));

const Navbar = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false); // Update this state based on login status

  return (
    <div>
      <AppBar position="static" className="Appbar" style={{ backgroundColor: '#3f51b5' }}>
        <Toolbar>
          <IconButton edge="start" color="inherit" aria-label="menu">
            <ShoppingCart />
          </IconButton>
          <Typography variant="h6">upGrad E-shop </Typography>
          <div style={{ flexGrow: 1 }} />

          {isLoggedIn && (
            <Search>
              <SearchIconWrapper>
                <SearchIcon />
              </SearchIconWrapper>
              <StyledInputBase placeholder="Search…" inputProps={{ 'aria-label': 'search' }} />
            </Search>
          )}

          <div style={{ marginLeft: 'auto' }}>
            <Link to="/Loginpage" className="login-link">
              Login
            </Link>
            <Link to="/Signuppage" className="Signup-link">
              SignUp
            </Link>
          </div>
          <div></div>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default Navbar;
